<?php
include "layouts/header.php";
?>
<!-- right part of the middle portion starts here -->
<div class="middle-right">
	<div class="page-status">
		<h1>About Us:</h1>
		<h2><i onclick='window.location.href = "index.html" '> Home /</i> About Us:</h2>
	</div>
	<div class="about-content">
		<?php
		$sel = "SELECT * FROM about_us";
		$exe = mysqli_query($conn, $sel);
		$data = mysqli_fetch_assoc($exe);
		echo $data['content'];
		?>
	</div>
</div>
<!-- right part of the middle portion starts here -->
<div class="clear"></div>
</div>
<?php
include "layouts/footer.php";
?>