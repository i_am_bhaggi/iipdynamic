<?php
require "../app/helper.php";
$msg = "";
$flag = 1;
$id = $_GET['id'];
// 1 all okay, 0: some error
if (isset($_POST['submit'])) {
    $desc = $_POST['desc'];
    if ($desc != "") {
        $qry = "UPDATE about_us SET content = '$desc', updated_at = current_timestamp()";
        $flag = mysqli_query($conn, $qry);
        if ($flag == 1) {
            $msg = "Data updated successfully";
        } else {
            $msg = "Unable to update data right now!! Internal server error";
        }
    } else {
        // throw an error
        $flag = 0;
        $msg = "Please fill all the required fields";
    }
}
$sel = "SELECT * FROM about_us";
$exe = mysqli_query($conn, $sel);
$data = mysqli_fetch_assoc($exe);
include("layouts/header.php");
?>
<!--Header Ends-->

<!-- Right Portion Starts-->
<div class="col-md-10 col-sm-10 right_menu">
    <div class="container-fluid">
        <div class="container" style="width: 90%;">
            <div class="row" id="details">
                <h2 class="col-12 text-center">Update About Us</h2>
                <div class="col-12">
                    <?php
                    if ($msg != "") {
                    ?>
                        <div class="alert alert-<?php echo $flag == 1 ? 'success' : 'danger' ?>">
                            <?php echo $msg ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div style="padding: 15px; box-shadow:0px 0px 5px grey;border-radius:5px">
                    <form action="" method="post">
                        <div class="col-12">
                            <label for="">Description</label>
                            <textarea id="editor" name="desc" id="" cols="30" rows="10" class="form-control"><?php echo $data['content']; ?></textarea>
                        </div>
                        <div class="col-12" style="margin-top:10px;">
                            <button class="btn btn-success" name="submit" type="submit">Update</button>
                            <button class="btn btn-warning" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Right Portion Ends-->
</div>
</div>
</div>
<!--Side Menu-->
<?php include("layouts/footer.php") ?>