<?php
require "../app/helper.php";
$error = 0;
$msg = "";
$id = $_GET['id'];
if ($id != "") {
    // get the data of this id from database
    $sel = "SELECT * FROM news WHERE course_id = $id";
    $exe = mysqli_query($conn, $sel);
    $data = mysqli_fetch_assoc($exe);
}
if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $description = $_POST['description'];
    $fees = $_POST['fees'];
    $theme_color = $_POST['theme_color'];
    if ($name != "" && $description != "") {
        $imageArray = $_FILES['image'];
        $ext = getExt($imageArray['name']);
        $courseIcon = getRandName($ext);
        $path = "../images/courses/";
        $dest = $path . $courseIcon;
        $fileFlag = move_uploaded_file($imageArray['tmp_name'], $dest);
        // validation pass   
        if ($fileFlag == true) {
            if ($id == "") {
                // insert query
                $ins = "INSERT INTO courses SET 
                                    course_name = '$name', 
                                    course_desc = '$description',
                                    course_fees = $fees,
                                    theme_color = '$theme_color',
                                    course_icon = '$courseIcon'";
                $flag =  mysqli_query($conn, $ins);
            }
            // else {
            //     // 
            //     $upd = "UPDATE news SET course_name	= '$name', course_desc = '$description', updated_at = current_timestamp() WHERE course_id = $id";
            //     $flag =  mysqli_query($conn, $upd);
            // }
        } else {
            $error = 1;
            $msg = "Unable to upload the image, internal server error!!!";
        }
        if ($flag == true) {
            header("Location:view-news.php");
        } else {
            // echo "<pre>";
            // $errorList = mysqli_error_list($conn);
            // print_r($errorList);
            // die;
            $error = 1;
            $msg = "Please try again, internal server error!!!";
        }
    } else {
        // validation failed
        $error = 1;
        $msg = "Please fill out all the fileds";
    }
}

include("layouts/header.php");
?>
<!--Header Ends-->

<!-- Right Portion Starts-->
<div class="col-md-10 col-sm-10 right_menu">
    <div class="container-fluid">
        <div class="container" style="width: 90%;">
            <div class="row" id="details">
                <h2 class="text-center"><?php echo $id == "" ? "Add" : "Update"; ?> Course</h2>
                <?php
                if ($msg != "") {
                    if ($error == 1) {
                ?>
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Oops!!</h4>
                            <p><?php echo $msg ?></p>
                        </div>
                    <?php
                    } else {
                    ?>
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Congo!!</h4>
                            <p><?php echo $msg ?></p>
                        </div>
                <?php
                    }
                }
                ?>
                <div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div style="box-shadow: 0px 0px 5px grey; padding:30px">
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="col-12">
                                        <label for="">
                                            Name
                                        </label>
                                        <input type="text" class="form-control" name="name" value="<?php echo $id == "" ? $name : $data['course_name']; ?>">
                                    </div>
                                    <div class="col-12">
                                        <label for="">
                                            Fees
                                        </label>
                                        <input type="number" class="form-control" name="fees" value="<?php echo $id == "" ? $fees : $data['course_fees']; ?>">
                                    </div>
                                    <div class="col-12">
                                        <label for="">
                                            Color
                                        </label>
                                        <input type="color" class="form-control" name="theme_color" value="<?php echo $id == "" ? $theme_color : $data['theme_color']; ?>">
                                    </div>
                                    <div class="col-12" style="margin-top: 10px;">
                                        <label for="">
                                            Description
                                        </label>
                                        <textarea class="form-control" name="description" id="editor" cols="30" rows="10"><?php echo $id == "" ? $description : $data['course_desc']; ?></textarea>
                                    </div>
                                    <div class="col-12" style="margin-top: 10px;">
                                        <label for="">
                                            Image
                                        </label>
                                        <input type="file" name="image" class="dropify" data-allowed-file-extensions="png jpg jpeg" />
                                    </div>
                                    <button class="btn btn-primary" name="submit" type="submit" style="margin-top: 10px;">
                                        <?php
                                        echo $id == "" ? "Add" : "Update";
                                        ?>
                                    </button>
                                    <button class="btn btn-danger" type="reset" style="margin-top: 10px;">Clear</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Right Portion Ends-->
</div>
</div>
</div>
<!--Side Menu-->
<?php include("layouts/footer.php") ?>