<?php
require "../app/helper.php";
$msg = "";
$flag = 1;
$id = $_GET['id'];
// 1 all okay, 0: some error
if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $desc = $_POST['desc'];
    if ($title != "" && $desc != "") {
        if (!is_null($id)) {
            $qry = "UPDATE news SET news_title = '$title', news_description = '$desc', updated_at = current_timestamp() WHERE news_id = $id";
        } else {
            $qry = "INSERT INTO news SET news_title = '$title', news_description = '$desc'";
        }
        $flag = mysqli_query($conn, $qry);
        if ($flag == 1) {
            header("LOCATION:view-news.php");
        } else {
            if (!is_null($id)) {
                $msg = "Unable to update data right now!! Internal server error";
            } else {
                $msg = "Unable to add data right now!! Internal server error";
            }
        }
    } else {
        // throw an error
        $flag = 0;
        $msg = "Please fill all the required fields";
    }
}
if (!is_null($id)) {
    $sel = "SELECT * FROM news WHERE news_status = 1 AND news_id = $id";
    $exe = mysqli_query($conn, $sel);
    $data = mysqli_fetch_assoc($exe);
    // p($data);
}
include("layouts/header.php");
?>
<!--Header Ends-->

<!-- Right Portion Starts-->
<div class="col-md-10 col-sm-10 right_menu">
    <div class="container-fluid">
        <div class="container" style="width: 90%;">
            <div class="row" id="details">
                <h2 class="col-12 text-center"><?php echo is_null($id) ? 'Add' : 'Update' ?> News</h2>
                <div class="col-12">
                    <?php
                    if ($msg != "") {
                    ?>
                        <div class="alert alert-<?php echo $flag == 1 ? 'success' : 'danger' ?>">
                            <?php echo $msg ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div style="padding: 15px; box-shadow:0px 0px 5px grey;border-radius:5px">
                    <form action="" method="post" onsubmit="return validateNewsForm(this)">
                        <div class="col-12">
                            <label for="">Title</label>
                            <input type="text" value="<?php echo $data['news_title'] ?>" class="form-control" name="title" autofocus="" />
                        </div>
                        <div class="col-12">
                            <label for="">Description</label>
                            <textarea id="editor" name="desc" id="" cols="30" rows="10" class="form-control"><?php echo $data['news_description'] ?></textarea>
                        </div>
                        <div class="col-12" style="margin-top:10px;">
                            <button class="btn btn-success" name="submit" type="submit"><?php echo is_null($id) ? 'Save' : 'Update' ?></button>
                            <button class="btn btn-warning" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Right Portion Ends-->
</div>
</div>
</div>
<!--Side Menu-->
<?php include("layouts/footer.php") ?>
<script>
    function validateNewsForm(form) {
        console.log(form.title.value, form.desc.value)
        if (form.title.value == "") {
            form.title.focus()
            console.log("error")
            return false;
        }
        return true;

    }
</script>