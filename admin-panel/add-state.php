<?php
require "../app/helper.php";
$msg = "";
$flag = 1;
$id = $_GET['id'];
// 1 all okay, 0: some error
if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $country_id = $_POST['country_id'];
    if ($title != "") {
        if (!is_null($id)) {
            $qry = "UPDATE states SET state_name = '$title', country_id = $country_id WHERE state_id = $id";
        } else {
            $qry = "INSERT INTO states SET state_name = '$title', country_id = $country_id";
        }
        $flag = mysqli_query($conn, $qry);
        if ($flag == 1) {
            header("LOCATION:view-country.php");
        } else {
            if (!is_null($id)) {
                $msg = "Unable to update data right now!! Internal server error";
            } else {
                $msg = "Unable to add data right now!! Internal server error";
            }
        }
    } else {
        // throw an error
        $flag = 0;
        $msg = "Please fill all the required fields";
    }
}
if (!is_null($id)) {
    $sel = "SELECT * FROM states WHERE state_id = $id";
    $exe = mysqli_query($conn, $sel);
    $data = mysqli_fetch_assoc($exe);
}
$countriesData = getCountries(0);
include("layouts/header.php");
?>
<!--Header Ends-->

<!-- Right Portion Starts-->
<div class="col-md-10 col-sm-10 right_menu">
    <div class="container-fluid">
        <div class="container" style="width: 90%;">
            <div class="row" id="details">
                <h2 class="col-12 text-center"><?php echo is_null($id) ? 'Add' : 'Update' ?> State</h2>
                <div class="col-12">
                    <?php
                    if ($msg != "") {
                    ?>
                        <div class="alert alert-<?php echo $flag == 1 ? 'success' : 'danger' ?>">
                            <?php echo $msg ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div style="padding: 15px; box-shadow:0px 0px 5px grey;border-radius:5px">
                    <form action="" method="post" onsubmit="return validateForm(this)">
                        <div class="col-12">
                            <label for="">Title</label>
                            <input type="text" value="<?php echo $data['state_name'] ?>" class="form-control" name="title" autofocus="" />
                        </div>
                        <div class="col-12">
                            <label for="">Country</label>
                            <select name="country_id" id="" class="form-control">
                                <option value=""> Select a country</option>
                                <?php
                                foreach ($countriesData as $data) :
                                ?>
                                    <option value="<?php echo $data[0] ?>"><?php echo $data[1] ?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-12" style="margin-top:10px;">
                            <button class="btn btn-success" name="submit" type="submit"><?php echo is_null($id) ? 'Save' : 'Update' ?></button>
                            <button class="btn btn-warning" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Right Portion Ends-->
</div>
</div>
</div>
<!--Side Menu-->
<?php include("layouts/footer.php") ?>
<script>
    function validateForm(form) {
        if (form.title.value == "") {
            form.title.focus()
            console.log("error")
            return false;
        }
        return true;

    }
</script>