<footer class="container-fluid footerbg">
	&copy; <?php echo date('Y') . ' - ' . (date('Y') + 1); ?>
	Reserved &nbsp;|&nbsp; Designed and Deveploped by:WsCube Tech
</footer>
<div id="snackbar"></div>
<!--Wrapper Ends-->
</body>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>
	CKEDITOR.replace('editor');
	setInterval(function() {
		$("#notification").slideUp(1000);
	}, 5000);

	function snackBar(message, status) {
		// Get the snackbar DIV
		var x = document.getElementById("snackbar");
		x.innerText = message;
		// Add the "show" class to DIV
		if (status == 1) {
			//success
			x.style.background = "green";
		} else {
			// danger
			x.style.background = "red";
		}
		x.className = "show";

		// After 3 seconds, remove the show class from DIV
		setTimeout(function() {
			x.className = x.className.replace("show", "");
		}, 5000);
	}
</script>
<script src="js/dropify.js"></script>
<script>
	var drEvent = $('.dropify').dropify();

	drEvent.on('dropify.beforeClear', function(event, element) {
		return confirm("Do you really want to delete \"" + element.filename + "\" ?");
	});
</script>

</html>