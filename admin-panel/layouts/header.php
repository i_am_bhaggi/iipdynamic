<?php
	if(!isset($_SESSION['admin_id'])){
		header("LOCATION:login.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>IIP | Admin | Dashboard </title>
	<link rel="icon" type="image/png" href="images/logo.png">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/dropify.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	<style>
		/* The snackbar - position it at the bottom and in the middle of the screen */
		#snackbar {
			visibility: hidden;
			/* Hidden by default. Visible on click */
			min-width: 250px;
			/* Set a default minimum width */
			margin-left: -125px;
			/* Divide value of min-width by 2 */
			background-color: #333;
			/* Black background color */
			color: #fff;
			/* White text color */
			text-align: center;
			/* Centered text */
			border-radius: 2px;
			/* Rounded borders */
			padding: 16px;
			/* Padding */
			position: fixed;
			/* Sit on top of the screen */
			z-index: 1;
			/* Add a z-index if needed */
			left: 50%;
			font-size: 20px;
			/* Center the snackbar */
			bottom: 30px;
			/* 30px from the bottom */
		}

		/* Show the snackbar when clicking on a button (class added with JavaScript) */
		#snackbar.show {
			visibility: visible;
			/* Show the snackbar */
			/* Add animation: Take 0.5 seconds to fade in and out the snackbar.
  However, delay the fade out process for 2.5 seconds */
			-webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
			animation: fadein 0.5s, fadeout 0.5s 2.5s;
		}

		/* Animations to fade the snackbar in and out */
		@-webkit-keyframes fadein {
			from {
				bottom: 0;
				opacity: 0;
			}

			to {
				bottom: 30px;
				opacity: 1;
			}
		}

		@keyframes fadein {
			from {
				bottom: 0;
				opacity: 0;
			}

			to {
				bottom: 30px;
				opacity: 1;
			}
		}

		@-webkit-keyframes fadeout {
			from {
				bottom: 30px;
				opacity: 1;
			}

			to {
				bottom: 0;
				opacity: 0;
			}
		}

		@keyframes fadeout {
			from {
				bottom: 30px;
				opacity: 1;
			}

			to {
				bottom: 0;
				opacity: 0;
			}
		}
	</style>
</head>

<body>

	<header class="container-fluid top">
		<div class="row">
			<div class="col-md-2 col-sm-3 col-xs-3 user_menu">
				<i class="fa fa-user" style="color:blue;">
					<?php echo $_SESSION['iip_admin_name']; ?>
				</i>&nbsp;
			</div>
			<div class="col-md-10 col-sm-9 col-xs-9 user">
				<div class="row">
					<a href="http://localhost/iip-academy/admin-panel">
						<div class="col-md-3 col-sm-3 col-xs-3 user_menu" title="Back To Dashboard">
							<i class="fa fa-home" style="color:black;"></i>
							Dashboard
						</div>
					</a>
					<a href="changepass.php">
						<div class="col-md-3 col-sm-3 col-xs-3 user_menu">
							<i class="fa fa-shield" style="color: orange"></i>
							Change Password
						</div>
					</a>
					<a href="logout.php">
						<div class="col-md-3 col-sm-3 col-xs-3 user_menu">
							<i class="fa fa-dot-circle-o" style="color: red"></i>
							Log-Out
						</div>
					</a>
					<a href="http://localhost/IIP" target="blank">
						<div class="col-md-3 col-sm-3 col-xs-3 user_menu">
							<img src="images/logo.png" style="width: 20px; height: 20px;">
							Go To Website
						</div>
					</a>
				</div>

			</div>
		</div>

	</header>
	<!--Side Menu-->
	<div class="container-fluid" style="padding-left: 0px;">
		<div class="container main">
			<div class="row">
				<h1 class="fa fa-align-justify" onclick="showmenu()"></h1>
				<!--Left Menu Starts-->
				<div class="col-md-2 col-sm-2 left_menu" id="showmenu">
					<div class="row">
						<h3 style="padding: 10px; text-align: center; color:white">
							Menu
						</h3>
						<span>
							<div class="col-md-12 col-sm-12 nav nav-bar menu left-menu-items has-children">
								<i class="fa fa-folder"></i>
								Pages
								<i class="fa fa-caret-down" style="float: right;"></i>
								<div class="children">
									<ul>
										<a href="about.php">
											<li>About Us</li>
										</a>
										<a href="contact.php">
											<li>Contact Us</li>
										</a>
									</ul>
								</div>
							</div>
						</span>
						<span>
							<div class="col-md-12 col-sm-12 nav nav-bar menu left-menu-items has-children">
								<i class="fa fa-folder"></i>
								Course
								<i class="fa fa-caret-down" style="float: right;"></i>
								<div class="children">
									<ul>
										<a href="add-course.php">
											<li>Add</li>
										</a>
										<a href="view-course.php">
											<li>View</li>
										</a>
									</ul>
								</div>
							</div>
						</span>
						<span>
							<div class="col-md-12 col-sm-12 nav nav-bar menu left-menu-items has-children">
								<i class="fa fa-folder"></i>
								Gallery
								<i class="fa fa-caret-down" style="float: right;"></i>
								<div class="children">
									<ul>
										<a href="add-gallery.php">
											<li>Add</li>
										</a>
										<a href="view-gallery.php">
											<li>View</li>
										</a>
									</ul>
								</div>
							</div>
						</span>
						</a>
						<span>
							<div class="col-md-12 col-sm-12 nav nav-bar menu left-menu-items has-children">
								<i class="fa fa-folder"></i>
								Country
								<i class="fa fa-caret-down" style="float: right;"></i>
								<div class="children">
									<ul>
										<a href="add-country.php">
											<li>Add</li>
										</a>
										<a href="view-country.php">
											<li>View</li>
										</a>
									</ul>
								</div>
							</div>
						</span>
						<span>
							<div class="col-md-12 col-sm-12 nav nav-bar menu left-menu-items has-children">
								<i class="fa fa-folder"></i>
								News
								<i class="fa fa-caret-down" style="float: right;"></i>
								<div class="children">
									<ul>
										<a href="add-news.php">
											<li>Add</li>
										</a>
										<a href="view-news.php">
											<li>View</li>
										</a>
									</ul>
								</div>
							</div>
						</span>
						<span>
							<div class="col-md-12 col-sm-12 nav nav-bar menu left-menu-items has-children">
								<i class="fa fa-folder"></i>
								State
								<i class="fa fa-caret-down" style="float: right;"></i>
								<div class="children">
									<ul>
										<a href="add-state.php">
											<li>Add</li>
										</a>
										<a href="view-state.php">
											<li>View</li>
										</a>
									</ul>
								</div>
							</div>
							</a>
					</div>
				</div>
				<!--Left Menu Ends-->