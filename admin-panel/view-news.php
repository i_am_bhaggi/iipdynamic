<?php
require "../app/helper.php";
$msg = "";
$flag = 1;

$search = $_GET['search'] ?? "";
$limit = 5;
$page = $_GET['page'] ?? 0;
// dev -> 0,1,2,3
// user -> 1,2,3,4
$start = $limit * $page;
$id = $_GET['id'] ?? null;
$status = $_GET['status'];
// null safe operator
if (!is_null($id)) {
    if (!is_null($status)) {
        // update query
        $upd = "UPDATE news SET news_status = $status WHERE news_id = $id";
        $flag = mysqli_query($conn, $upd);
        if ($flag) {
            $msg = "Status changed successfully";
        } else {
            $msg = "Unable to change the status";
        }
        // Syntax: Update table_name SET col1 = val1, col2 = val2, ... coln = valn;
    } else {
        $del = "DELETE FROM news WHERE news_id = $id";
        $flag = mysqli_query($conn, $del);
        if ($flag) {
            $msg = "Data deleted successfully";
        } else {
            $msg = "Unable to delete the data";
        }
    }
}
if (isset($_POST['delete'])) {
    // multiple delete query
    $ids = $_POST['ids'];
    if (count($ids) != 0) {
        foreach ($ids as $id) {
            $del = "DELETE FROM news WHERE news_id = $id";
            $flag = mysqli_query($conn, $del);
            if ($flag == false) {
                break;
            }
        }
        if ($flag) {
            $msg = "Data deleted successfully";
        } else {
            $msg = "Unable to delete the data";
        }
    }
} elseif (isset($_POST['change'])) {
    $ids = $_POST['ids'];
    if (count($ids) != 0) {
        foreach ($ids as $id) {
            $data = mysqli_fetch_assoc(mysqli_query($conn, "SELECT news_status FROM news WHERE news_id = $id"));
            $oldStatus = $data['news_status'];
            $newStatus = $oldStatus == "1" ? "0" : "1";
            $upd = "UPDATE news SET news_status = $newStatus WHERE news_id = $id";
            $flag = mysqli_query($conn, $upd);
            if ($flag == false) {
                break;
            }
        }
        if ($flag) {
            $msg = "Status changed successfully";
        } else {
            $msg = "Unable to change the status";
        }
    }
}


include("layouts/header.php");
?>
<!--Header Ends-->

<!-- Right Portion Starts-->
<div class="col-md-10 col-sm-10 right_menu">
    <div class="container-fluid">
        <div class="container" style="width: 90%;">
            <div class="row" id="details">
                <h2 class="col-12 text-center">View News</h2>
                <div class="col-12">
                    <!-- searching form -->
                    <form>
                        <input type="search" name="search" id="" class="form-control" value="<?php echo $search ?>" />
                        <button class="btn btn-success" type="submit">Search</button>
                        <a href="view-news.php"> <button class="btn btn-danger" type="button">Clear</button></a>
                    </form>
                </div>
                <div class="col-12" style="margin-bottom: 10px;">
                    <?php
                    if ($msg != "") {
                    ?>
                        <div class="alert alert-<?php echo $flag == 1 ? 'success' : 'danger' ?>">
                            <?php echo $msg ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
                <div style="padding: 15px; box-shadow:0px 0px 5px grey;border-radius:5px">
                    <!--                     
                     * mysqli_fetch_array() 
                     * mysqli_fetch_assoc() 
                     * mysqli_fetch_all
                     * -->
                    <form action="" method="post">
                        <button class="btn btn-primary" type="submit" name="delete">Delete checked</button>
                        <button class="btn btn-primary" type="submit" name="change">Toggle checked</button>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" name="" id="check-all" /> <br /> All
                                    </th>
                                    <th>Sr No.</th>
                                    <th>Title</th>
                                    <th width="40%">Description</th>
                                    <th>Created Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // prepare a query
                                //  ORDER BY news_id DESC            
                                $sel = "SELECT * FROM news";
                                if ($search != "")
                                    $sel .= " WHERE news_title LIKE '%$search%'";
                                $sel .= " LIMIT $start,$limit";
                                // DESC or ASC
                                $exe = mysqli_query($conn, $sel);
                                $sr = 1;
                                while ($data = mysqli_fetch_assoc($exe)) :
                                ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="ids[]" id="" value="<?php echo $data['news_id'] ?>" class="checkbox" />
                                        </td>
                                        <td><?php echo $data['news_id'] ?></td>
                                        <td><?php echo $data['news_title'] ?></td>
                                        <td>
                                            <?php echo $data['news_description'] ?>
                                        </td>
                                        <td>
                                            <?php echo formatDate($data['created_at']) ?>
                                            <br />
                                            <?php echo formatDate($data['created_at'], "h:i a") ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($data['news_status'] == "1") :
                                            ?>
                                                <a href="view-news.php?status=0&id=<?php echo $data['news_id']; ?>&page=<?php echo $page; ?>&search=<?php echo $search ?>">
                                                    <button type="button" class="btn btn-success"> Active </button>
                                                </a>
                                            <?php else : ?>
                                                <a href="view-news.php?status=1&id=<?php echo $data['news_id']; ?>&page=<?php echo $page; ?>&search=<?php echo $search ?>">
                                                    <button type="button" class="btn btn-warning"> Inactive </button>
                                                </a>
                                            <?php
                                            endif
                                            ?>
                                        </td>
                                        <td>
                                            <a href="view-news.php?id=<?php echo $data['news_id']; ?>&page=<?php echo $page; ?>&search=<?php echo $search ?>  ">
                                                <button type="button">
                                                    <i class="fa fa-trash" style="color:red"></i>
                                                </button>
                                            </a>
                                            <a href="add-news.php?id=<?php echo $data['news_id']; ?>">
                                                <button type="button">
                                                    <i class="fa fa-pencil" style="color:blue"></i>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                    $sr++;
                                endwhile
                                ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- total rows -->
                <?php
                $rowSel = "SELECT count(news_id) as total_rows FROM news";
                if ($search != "")
                    $rowSel .= " WHERE news_title LIKE '%$search%'";

                $rowExe = mysqli_query($conn, $rowSel);
                $totalData = mysqli_fetch_assoc($rowExe);
                $totalRow = $totalData['total_rows'];
                $totalPage = ceil($totalRow / $limit);
                ?>
                <div class="col-12">
                    <nav aria-label="...">
                        <ul class="pagination">
                            <li class="disabled">
                                <a href="view-news.php?page=" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a>
                            </li>
                            <?php
                            for ($pageNo = 0; $pageNo < $totalPage; $pageNo++) :
                            ?>
                                <li class="<?php echo $pageNo == $page ? 'active' : '' ?>">
                                    <a href="view-news.php?page=<?php echo $pageNo; ?>&search=<?php echo $search ?>"><?php echo $pageNo + 1; ?></a>
                                </li>
                            <?php
                            endfor
                            ?>
                            <li>
                                <a href="view-news.php?page=" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Right Portion Ends-->
</div>
</div>
</div>
<!--Side Menu-->
<?php include("layouts/footer.php") ?>
<script>
    $("#check-all").change(
        function() {
            var flag = $(this).prop("checked") //get 
            $(".checkbox").prop("checked", flag); //set
        }
    )
</script>