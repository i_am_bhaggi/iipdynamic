<?php
// connection creation
$conn = mysqli_connect("localhost", "root", "", "iip_dynamic") or die("Connection error");


function getNews()
{
    global $conn;
    $sel = "SELECT * FROM news WHERE news_status = 1 ORDER BY news_id DESC";
    $exe = mysqli_query($conn, $sel);
    $data = mysqli_fetch_all($exe);
    return $data;
}

function getCountries($all = 1)
{
    global $conn;
    if ($all == 1) {
        $sel = "SELECT * FROM countries";
    } else {
        $sel = "SELECT * FROM countries WHERE country_status = 1";
    }
    $exe = mysqli_query($conn, $sel);
    $data = mysqli_fetch_all($exe);
    return $data;
}
