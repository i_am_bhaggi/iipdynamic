<?php
date_default_timezone_set("ASIA/KOLKATA");
error_reporting(0);
session_start();
include "database.php";


function p($data, $exit = 0)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($exit == 1)
        exit;
}

/**
 * @param date $date, string $format
 * @return date
 */
function formatDate($date, $format = "d-M-Y")
{
    $timeStamp = strtotime($date);
    return date($format, $timeStamp);
}


function getExt($fileName)
{
    $nameArr = explode(".", $fileName);
    return end($nameArr);
}

function getRandName($ext)
{
    return time() . uniqid(rand(), false) . "." . $ext;
}
