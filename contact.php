<?php
include "layouts/header.php";
?>
<!-- right part of the middle portion starts here -->
<div class="middle-right">

	<div class="page-status">
		<h1>Contact</h1>
		<h2><i onclick='window.location.href = "index.html" '> Home /</i> Contact</h2>
	</div>
	<div class="contact-content">
		<div class="contact-row">
			<div class="contact-row-left">
				<img src="images/phone.png">
				<p><b>Phone:</b> 0291-2468122, +91-9269698122</p>
				<p> <b>Email ID:</b>
					<a href="http://www.iipacademy.com/" target="blank">
						<font style="color:#00a8ec; cursor: pointer;">info@iipacademy.com </font>
					</a>
				</p>
				<p> <b>Website:</b>
					<a href="http://www.iipacademy.com" target="blank">
						<font style="color:#00a8ec; cursor: pointer;">www.iipacademy.com </font>
					</a>
				</p>
			</div>
			<div class="contact-row-right">
				<img src="images/address-pin.png">
				<p>
					Ground Floor, Laxmi Tower, Bhaskar Circle, Ratanada, Jodhpur (Raj.)
				</p>
			</div>

		</div>
		<h1 style="color: #343130;">Find Us On Map</h1>
		<div class="contact-map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3577.632444502631!2d73.02830631486876!3d26.27358998340699!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39418c37b277d1c3%3A0x1412272be9646840!2sWsCube%20Tech!5e0!3m2!1sen!2sin!4v1642071545023!5m2!1sen!2sin" width="800" height="370" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>

	</div>
</div>
<!-- right part of the middle portion starts here -->
<div class="clear"></div>
</div>
<?php
include "layouts/footer.php";
?>