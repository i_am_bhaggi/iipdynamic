<?php
include "layouts/header.php";
$couSel = "SELECT * FROM courses WHERE course_status = 1 ORDER BY course_name ASC";
$couExe = mysqli_query($conn, $couSel);
?>
<!-- right part of the middle portion starts here -->
<div class="middle-right">
	<div class="page-status">
		<h1>Courses</h1>
		<h2><i onclick='window.location.href = "index.html" '> Home /</i> Courses</h2>
	</div>
	<div class="course-content">
		<div class="row1">
			<?php
			while ($course = mysqli_fetch_assoc($couExe)) :
				$themeColor = $course['theme_color'];
			?>
				<div class="course-1">
					<div class="course-1-icon" style="background:<?php echo $themeColor; ?>">
						<div class="icon-1">
							<img src="images/courses/<?php echo $course['course_icon']; ?>">
						</div>
					</div>

					<div class="c-1" style="color:<?php echo $themeColor; ?>;border-color:<?php echo $themeColor; ?>">
						<?php echo $course['course_name'] ?>
					</div>
				</div>
			<?php
			endwhile;
			?>
		</div>
	</div>
</div>
<!-- right part of the middle portion starts here -->
<div class="clear"></div>
</div>
<?php
include "layouts/footer.php";
?>