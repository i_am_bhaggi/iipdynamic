<?php
include "layouts/header.php";
$countriesData = getCountries(0);
?>
<!-- right part of the middle portion starts here -->
<div class="middle-right enq-height">
	<div class="page-status">
		<h1>Enquiry</h1>
		<h2><i onclick='window.location.href = "index.html" '> Home /</i> Enquiry</h2>
	</div>
	<div class="mainwebsitecontent">
		<form id="enquiry-form">
			<div class="formrow">
				<div class="formlable">Gender : </div>
				<div class="inputform">
					<input type="radio" name="gender" value="M" id="male" class="" />Male
					<input type="radio" name="gender" value="F" id="female" class="" />Female
				</div>
			</div>
			<div class="formrow">
				<div class="formlable">Name : </div>
				<div class="inputform"><input type="text" name="name" id="name" class="inputbox" /></div>
			</div>
			<div class="formrow">
				<div class="formlable">Contact No : </div>
				<div class="inputform"><input type="text" name="contact" id="contact" class="inputbox" /></div>
			</div>
			<div class="formrow">
				<div class="formlable">Country : </div>
				<div class="inputform">
					<select name="country" id="country">
						<option value=""> Select a country</option>
						<?php
						foreach ($countriesData as $data) :
						?>
							<option value="<?php echo $data[0] ?>"><?php echo $data[1] ?></option>
						<?php
						endforeach;
						?>
					</select>
				</div>
			</div>
			<div style="text-align:center">
				<img src="images/loading-buffering.gif" alt="" width="30" id="loader" style="display: none;" />
			</div>
			<div class="formrow">
				<div class="formlable">State : </div>
				<div class="inputform">
					<select name="state" id="state" disabled>
						<option value="">--Select a state--</option>
					</select>
				</div>
			</div>
			<div class="formrow">
				<div class="formlable">Address : </div>
				<div class="inputform">
					<textarea name="address" id="address" class="textarea"></textarea>
				</div>
			</div>
			<div class="formrow">
				<div class="formlable">Email : </div>
				<div class="inputform"><input type="text" name="email" id="email" class="inputbox" /></div>
			</div>
			<div class="formrow">
				<div class="formlable">Enquiry : </div>
				<div class="inputform"><textarea name="enquiry" id="enquiry" class="textarea"></textarea></div>
			</div>
			<div class="formrow">
				<div class="formlable"><input type="submit" value="Send" class="button" /></div>
			</div>
		</form>
	</div>
</div>
<!-- right part of the middle portion starts here -->
<div class="clear"></div>
</div>
<!-- middle portion with  links, new , banner and course ends here -->
<?php
include "layouts/footer.php";
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
	$("#country").change(
		function() {
			const cId = $(this).val();
			$.ajax({
				url: "state-ajax.php",
				type: "get",
				data: {
					country_id: cId
				},
				beforeSend: function() {
					$("#loader").fadeIn();
					$("#state").html("<option> Loading data... </option>").attr("disabled", true);
				},
				success: function(res) {
					$("#state").html(res).attr("disabled", false);
					$("#loader").fadeOut();
				}
			})
		}
	);

	$("#enquiry-form").submit(
		function() {
			var enquiryForm = $(this);
			var fromData = enquiryForm.serialize();
			$.ajax({
				url: "enquiry-ajax.php",
				data: fromData,
				method: "post",
				success: function(response) {
					const jsonRes = JSON.parse(response);
					if (jsonRes.flag == true) {
						enquiryForm.trigger("reset");
						swal("Good job!", jsonRes.msg, "success");
					} else {
						swal("Oops!", jsonRes.msg, "error");
					}
				}
			})
			return false;
		}
	)
</script>