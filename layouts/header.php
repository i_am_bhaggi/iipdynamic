<?php
include "app/helper.php";
?>
<!DOCTYPE html>
<html>

<head>
    <title>IIP</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
    <div class="main">
        <div class="top-contact">
            <div class="left-cont">
                <img src="images/web.png" class="top-left-img" />
                <div class="top-left-iip">
                    <a href="http://www.iipacademy.com" title="www.iipacademy.com">www.iipacademy.com </a>
                </div>
            </div>
            <div class="right-cont">
                <img src="images/info.png" class="top-right-img" />
                <div class="top-right-info">
                    <a href="mailto:info@iipacademy.com" title="info@iipacademy.com">info@iipacademy.com</a>
                </div>
            </div>
        </div>
        <div class="menu-container">
            <a href="index.php"><img src="images/logo.png" class="logo"></a>
            <div class="menu-bar">
                <div class="active-menu-item">
                    <a href="index.php" title="Home">Home</a>
                </div>
                <div class="menu-items" title="About us">
                    <a href="about.php" title="About us">About Us</a>
                </div>
                <div class="menu-items">
                    <a href="course.php">Courses</a>
                </div>
                <div class="menu-items">
                    <a href="gallery.php">Gallery</a>
                </div>
                <div class="menu-items">
                    <a href="enquiry.php">Enquiry</a>
                </div>
                <div class="menu-items">
                    <?php
                    if (isset($_SESSION['user_id'])) :
                    ?>
                        <a href="logout.php">Logout</a>
                    <?php
                    else :
                    ?>
                        <a href="login.php">Login</a>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <!-- middle portion with  links, new , banner and course starts here -->
        <div class="middle-container">
            <!-- left part of the middle portion starts here -->
            <div class="middle-left">
                <div class="menu-item-left">
                    <a href="index.php">Home</a>
                </div>
                <div class="menu-item-left">
                    <a href="about.php">About Us</a>
                </div>
                <div class="menu-item-left">
                    <a href="course.php">Courses</a>
                </div>
                <div class="menu-item-left">
                    <a href="gallery.php">Gallery</a>
                </div>
                <div class="menu-item-left">
                    <a href="enquiry.php">Enquiry</a>
                </div>
                <div class="menu-item-left">
                    <a href="contact.php">Contact</a>
                </div>
                <div class="middle-left-buttom">
                    <div class="middle-left-buttom-news">
                        News
                    </div>
                    <ul class="news-list">
                        <?php
                        $newsData = getNews();
                        foreach ($newsData as $news) :
                        ?>
                            <li>
                                <img src="images/dot.jpg">
                                <a href="read-news.php?id=<?php echo $news[0] ?>"><?php echo $news[1] ?></a>
                            </li>
                        <?php
                        endforeach
                        ?>
                    </ul>
                </div>
            </div>
            <!-- left part of the middle portion ends here -->