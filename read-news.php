<?php
include "layouts/header.php";
$id = $_GET['id'];
if(is_null($id)){
    header("LOCATION:index.php");
}else{
    // get the data
    $sel = "SELECT * FROM news WHERE news_status = 1 AND news_id = $id";
    $exe = mysqli_query($conn,$sel);
    $data = mysqli_fetch_assoc($exe);  
    // p($data);
}
?>
<!-- right part of the middle portion starts here -->
<div class="middle-right">
    <div class="page-status">
        <h1><?php echo $data['news_title'] ?? 'News does not exist'?></h1>
        <h2><i onclick='window.location.href = "index.html" '> Home /</i> Read News:</h2>
    </div>
    <div class="about-content">
        <?php echo $data['news_description']?>
    </div>
</div>
<!-- right part of the middle portion starts here -->
<div class="clear"></div>
</div>
<?php
include "layouts/footer.php";
?>