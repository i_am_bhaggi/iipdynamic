<?php
include "app/helper.php";
$name = $_POST['name'];
$email = $_POST['email'];
$password = $_POST['password'];
$c_password = $_POST['c_password'];

if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    if ($name != "" && $email != "" && $password != "" && $c_password != "") {
        if ($password == $c_password) {
            $sel = "SELECT * FROM users WHERE user_email = '$email'";
            $count = mysqli_num_rows(mysqli_query($conn, $sel));
            if ($count > 0) {
                $msg = "User already exists";
                $flag = false;
            } else {
                $ins = "INSERT INTO users SET user_name = '$name',user_email = '$email', user_password = '$password'";
                $flag = mysqli_query($conn, $ins);
                if ($flag) {
                    $msg = "Registration successful";
                } else {
                    $msg = "Unable to register! Please try again later";
                }
            }
        } else {
            $msg = "Password and confirm password must match";
            $flag = false;
        }
    } else {
        $msg = "Please fill all the required fields";
        $flag = false;
    }
} else {
    $msg = "Please enter a valid email";
    $flag = false;
}


$res = compact('msg', 'flag');

echo json_encode($res);
